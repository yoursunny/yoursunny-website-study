---
layout: course
courseid: IS494
title: "大型系统软件课程设计"
school: "上海交通大学信息安全专业"
instructor: "陈凯"
---

## hProxyN 精巧型HTTP代理服务器

hProxyN是一个C#编写的精巧型HTTP代理服务器。设计思路明确，模块结构清晰，扩展接口完善，配置选项丰富，管理操作方便，执行效率较高，程序运行稳定。

hProxyN功能介绍

* 普通网民可以用hProxyN做到——

    * 观看土豆/优酷等flv视频时，同步保存到硬盘
    * 多机共享，全家上网；利用代理身份认证和访问控制功能自定义上网权限
    * 使用普通浏览器访问WML手机网页

* 网站/服务器管理员可以用hProxyN做到——

    * 模拟IE浏览器访问指定网址，自动抓取包括脚本、Flash在内的所有资源，产生日志交由脚本分析，及时发现网页链接错误
    * 自定义DNS地址、自定义服务器IP地址，指定访问服务器集群/CDN的任何一个节点

* 其他实验性功能——

  * Web服务器：虚拟主机网站服务器，支持静态网页、PHP脚本、ASP.Net 3.5应用
  * 广告植入：在网页的不同位置植入广告
  * 系统服务：能够作为Windows Service后台运行，并在“性能监视器”中发布统计数据
  * 教育网加速：调用GAppProxy的fetchServer服务器，使教育网用户快速访问国外网站

hProxyN源代码 <https://github.com/yoursunny/code2014/tree/hProxyN>

实验报告
[软件需求规格说明](hProxyN-req.pdf)
[软件(结构)设计说明](hProxyN-structure.pdf)
[项目介绍讲稿](20080918.pdf)
[关键技术讲稿](20081009.pdf)
[产品设计讲稿](20081023.pdf)
