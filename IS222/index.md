---
layout: course
courseid: IS222
title: "嵌入式系统原理与应用"
school: "上海交通大学信息安全专业"
instructor: "刘海涛"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS222
downloads:
    - "00说明.ppt"
    - "01嵌入式系统概述.ppt"
    - "01嵌入式系统硬件.ppt"
    - "01嵌入式系统软件.ppt"
    - "02嵌入式处理器ARM.ppt"
    - "02嵌入式系统Bootloader.ppt"
    - "03系统级设计.ppt"
    - "03软硬件协同划分技术.ppt"
    - "03软硬件协同设计技术.ppt"
    - "04 Linux嵌入式系统设计.ppt"
    - "04 Linux嵌入式系统设计_开发环境搭建.ppt"
    - "05 MP3播放器设计.ppt"
    - "05其他系统.ppt"
    - "05工程机械智能监控器设计.ppt"
    - "05网络存储.ppt"
---

* [复习提纲](outline.pdf)
* 作业一：[嵌入式系统市场 之 家用游戏主机](game-console.pdf)
* 作业二：[Linux@ARM](linux-arm.pdf)
