---
layout: course
courseid: IS220
title: "信息安全管理体系及技术"
school: "上海交通大学信息安全专业"
instructor: "张少俊"
---

引用小磊的口头禅：**没有我你就挂了**～enjoy

## 互动答题

* [信息安全管理体系 习题](p1.htm)
* [网络安全管理协议 习题](p2.htm)
* [网络安全事件管理 习题](p3.htm)
* [网络安全策略管理 习题](p4.htm)

