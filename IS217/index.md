---
layout: course
courseid: IS217
title: "计算机病毒原理"
school: "上海交通大学信息安全专业"
instructor: "刘功申"
---

## 查毒程序

[VirusScanner技术报告](VirusScanner.pdf)

### 作品特点

1. 交叉杀毒——在Linux下查杀Windows病毒，防止防病毒软件本身被感染
2. 光盘杀毒——光盘启动，网络下载更新病毒库查毒，在服务器辅助下杀毒；此功能未演示
3. SMA多模式匹配——采用基于二叉树的SMA多模式匹配算法，小内存一样能运行
4. 支持OpenAntiVirus病毒库格式导入——充分利用开源杀毒软件病毒库

### 编译方法

[VirusScanner查毒程序源代码](https://github.com/yoursunny/code2014/tree/VirusScanner)

请安装好下列开发工具

1. gcc 4.2.3
2. Code::Blocks 8.02

然后用CodeBlocks打开virus.workspace，编译各个工程

### 程序说明

本作品共包含四个程序

1. `credo`，将OpenAntiVirus病毒库转换成pattern-list
2. `smaconv`，通过pattern-list创建SMA二叉树
3. `scanner`，利用SMA二叉树对文件进行扫描
4. `result.sh`，显示扫描结果
