---
layout: course
courseid: EE305
title: "微机原理与接口技术"
school: "上海交通大学电子信息与电气工程学院"
instructor: "翁正新"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/EE305
downloads:
    - "01概论.pdf"
    - "02 8086系统结构.pdf"
    - "03 8086 8088的指令系统.pdf"
    - "04汇编语言程序设计.pdf"
    - "05存储器.pdf"
    - "06 IO接口和总线.ppt"
    - "07中断系统.pdf"
    - "08可编程计数器定时器8253及其应用.pdf"
    - "09可编程外围接口芯片8255A及其应用.pdf"
    - "10串行通信及其接口芯片8251A.pdf"
    - "11模数和数模转换.pdf"
    - "14 MCS-51单片机.pdf"
    - "复习提纲.ppt"
---
## 银行柜员机模拟器

8086汇编语言写成的银行柜员机模拟器。模拟ATM的界面，可以读写文本数据文件，支持50张卡。

{% youtube "https://www.youtube.com/watch?v=tj3sY_wXDD0" %}

[下载ATM模拟器源代码](https://github.com/yoursunny/code2014/tree/ATM)

* 开发环境: [未来汇编](https://baike.baidu.com/item/%E6%9C%AA%E6%9D%A5%E6%B1%87%E7%BC%96/9241214)
* 参考资料: [键盘扫描码](https://web.archive.org/web/20180526012114/http://www.barcodeman.com/altek/mule/scandoc.php)
  (提示：图片上的编号并不是最终的扫描码，要根据图片上的编号对应表格第一列查询；8086汇编应该看XT这列的扫描码)
* 参考资料: [INT 21H 中断表](https://blog.csdn.net/wongson/article/details/3679884)
