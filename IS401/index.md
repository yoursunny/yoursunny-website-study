---
layout: course
courseid: IS401
title: "移动通信"
school: "上海交通大学信息安全专业"
instructor: "吴越 易平"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS401
downloads:
    - "移动通信01 Introduction.pdf"
    - "移动通信02 Radio Propagation for Mobile Environment.pdf"
    - "移动通信03 Modulation.pdf"
    - "移动通信04 Equalization Diversity Channel Coding.pdf"
    - "移动通信05 Multiple Access.pdf"
    - "无线网络01无线网络与移动计算概述.ppt"
    - "无线网络02蜂窝移动通信系统.ppt"
    - "无线网络03移动性管理.ppt"
    - "无线网络04第三代移动通信系统.ppt"
    - "无线网络05移动通信展望（4G）.ppt"
    - "无线网络06移动IP.ppt"
    - "无线网络07移动数据网络.ppt"
    - "无线网络08无线局域网.ppt"
    - "无线网络09移动ad hoc网络.ppt"
    - "无线网络10无线MESH网络.ppt"
    - "无线网络11移动ad hoc网络安全.ppt"
    - "无线网络 无线MESH网络及研究项目简介.ppt"
---

* [TD-SCDMA Physical Layer Design Overview](TD-physical.pdf)
* [团队通信系统：用ad hoc和TD-SCDMA实现](TeamComm.pdf)
