---
layout: course
courseid: IS203
title: "编译原理"
school: "上海交通大学信息安全专业"
instructor: "张爱新 訾小超"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS203
downloads:
    - "01引言.ppt"
    - "02文法和语言.ppt"
    - "03词法分析.ppt"
    - "03词法分析_习题.ppt"
    - "04语法分析.ppt"
    - "04语法分析_习题.ppt"
    - "04语法分析_答案.doc"
    - "05语法制导翻译和中间代码生成.ppt"
    - "05语法制导翻译和中间代码生成_答案.doc"
    - "06运行时存储空间管理.ppt"
    - "07代码优化.ppt"
    - "07代码优化_习题.ppt"
    - "08代码生成.ppt"
---

[编译原理提纲](outline.htm)
