---
layout: course
courseid: IS491
title: "大型应用软件课程设计"
school: "上海交通大学信息安全专业"
instructor: "刘海涛"
---

## 图书管理信息系统

{% youtube "https://www.youtube.com/watch?v=GUhe93hgK2c" %}

[课程设计报告](library.pdf)
[演讲稿1](20080327.pdf)
[演讲稿2](20080410.pdf)
[演示系统源代码](https://github.com/yoursunny/code2014/tree/library)

## 个人电话本

[课程设计报告](phonebook.pdf)
[演示系统源代码](https://github.com/yoursunny/code2014/tree/PhoneBook)
