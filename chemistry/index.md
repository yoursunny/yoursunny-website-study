---
layout: course
title: "化学"
school: "上海市七宝中学"
instructor: "戴珊玲"
---

* [阳光男孩的高中化学笔记本](notebook/)
* [《有机化学实验》会考复习课件](organic-experiment/)
