---
layout: course
courseid: IS214
title: "数据库原理"
school: "上海交通大学信息安全专业"
instructor: "马进"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS214
downloads:
    - "01数据库系统概论.pdf"
    - "02关系模型和关系运算.pdf"
    - "03关系规范化基础.pdf"
    - "04结构化查询语言SQL.pdf"
    - "05数据库应用系统设计.pdf"
    - "06数据库安全性.pdf"
    - "数码商品进销存管理 演示程序.zip"
---

## 数码商品进销存管理

{% youtube "https://www.youtube.com/watch?v=Ad5sVJVT46c" %}

[数码商品进销存管理 课题报告](digi.htm)

[数码商品进销存管理 PHP源代码](https://github.com/yoursunny/code2014/tree/digi)
