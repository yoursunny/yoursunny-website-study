---
layout: course
courseid: IS205
title: "信息论与编码"
school: "上海交通大学信息安全专业"
instructor: "齐开悦"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS205
downloads:
    - "01绪论.ppt"
    - "02信源及信源熵.ppt"
    - "03无失真信源编码.ppt"
    - "04限失真信源编码.ppt"
    - "05信道编码a.ppt"
    - "05信道编码b分组码.ppt"
    - "05信道编码c循环码.ppt"
---
