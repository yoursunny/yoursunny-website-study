---
layout: course
courseid: IS201
title: "信息安全的数学基础"
school: "上海交通大学信息安全专业"
instructor: "邱卫东 陈恭亮"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS201
downloads:
    - "08群.pdf"
    - "09群的结构.pdf"
    - "10环.pdf"
    - "10 Z nZ问题.pdf"
    - "11域和Galois域.pdf"
    - "12域的结构.pdf"
    - "13椭圆曲线.pdf"
    - "课本.pdf"
    - "复习.pdf"
---

* [信息安全的数学基础 作业题](homework.htm)
* 我的作品 [分解质因数工具](divide_element.htm)
* [课程学习情况调查结果](survey.htm)

