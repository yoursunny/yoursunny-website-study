---
layout: course
courseid: EI223
title: "电磁场"
school: "上海交通大学电子信息与电气工程学院"
instructor: "何广强"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/EI223
downloads:
    - "01矢量分析.pdf"
    - "02a电磁场的基本方程和电磁场运动的基本规律.pdf"
    - "02b电磁场的基本方程和电磁场运动的基本规律.pdf"
    - "03a静态场.pdf"
    - "03b静态场.pdf"
    - "04平面电磁波.pdf"
    - "05导行电磁波.pdf"
    - "散度 旋度.ppt"
    - "电磁场能量和能流.doc"
---
