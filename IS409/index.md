---
layout: course
courseid: IS409
title: "下一代网络及软交换原理"
school: "上海交通大学信息安全专业"
instructor: "陈凯"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS409
downloads:
    - "1 语音网络.ppt"
    - "2 数据网络.ppt"
    - "3 NGN体系结构.ppt"
    - "4 NGN协议.ppt"
    - "5 NGN高级话题.ppt"
    - "课程安排.ppt"
---

* [ScoreBoard](ScoreBoard.htm)：基于UDP的“比分牌”协议
* [用Lua语言编写Wireshark dissector插件](/t/2008/Wireshark-Lua-dissector/)
* [用yate2实现软VoIP语音通话(SIP协议)](/t/2009/VoIP-yate2-SIP/)

