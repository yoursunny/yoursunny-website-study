---
layout: course
courseid: IS407
title: "现代密码学"
school: "上海交通大学信息安全专业"
instructor: "黄征"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS407
downloads:
    - "01概述.pdf"
    - "02古典加密.pdf"
    - "03分组加密技术.pdf"
    - "09保密通讯.pdf"
    - "10公钥密码RSA.pdf"
    - "11可证安全的公钥密码.pdf"
    - "12公钥密码ECC.pdf"
    - "13认证、散列函数、数字签名.pdf"
    - "16密钥管理.pdf"
    - "参考书 Lecture Notes on Cryptography麻省理工学院密码学讲义.pdf"
    - "参考书 密码编码学与网络安全：原理与实践 William Stllings 刘玉珍 王丽娜 傅建明.pdf"
    - "参考书 应用密码学 王衍波 薛通.pdf"
    - "参考书 现代密码学理论与实践 Wenbo Mao 王继林 伍前红.pdf"
    - "现代密码学 考试范围.txt"
    - "相同MD5值的不同程序.7z"
---
## 学习资料

* [现代密码学 样卷](p/)
* 古典加密-[恺撒密码加解密程序](Caesar.htm)
* 古典加密-[Hill密码加解密程序](Hill.htm)
