---
layout: course
courseid: IS410
title: "路由器原理"
school: "上海交通大学信息安全专业"
instructor: "刘军荣"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS410
downloads:
    - "01相关网络基础知识.ppt"
    - "02 IP寻址.ppt"
    - "03路由器基本配置.ppt"
    - "04静态路由.ppt"
    - "05动态路由选择协议.ppt"
    - "06 RIP.ppt"
    - "07 IGRP.ppt"
    - "08 RIPv2.ppt"
    - "09 EIGRP.ppt"
    - "10 OSPF.ppt"
    - "11 IS-IS.ppt"
    - "12路由重新分配.ppt"
    - "13缺省路由和按需路由选择.ppt"
    - "14路由过滤.ppt"
    - "15路由映射.ppt"
    - "TCP-IP卷1英文版.pdb"
    - "实验.ppt"
---
