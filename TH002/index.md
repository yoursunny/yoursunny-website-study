---
layout: course
courseid: TH002
title: "思想道德修养"
school: "上海交通大学人文学院"
instructor: "施索华"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/TH002
downloads:
    - "1概论.ppt"
    - "2中国传统的伦理道德思想.ppt"
    - "3西方国家的伦理道德思想.ppt"
    - "4关于社会冷漠.doc"
    - "4社会主义市场经济体制下的道德建设.ppt"
    - "5做一个坚定的爱国者.ppt"
    - "7大学生礼仪规范.ppt"
---
