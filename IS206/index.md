---
layout: course
courseid: IS206
title: "操作系统"
school: "上海交通大学信息安全专业"
instructor: "李小勇"

download_link: http://cid-1fa4aa816e7dc9f3.skydrive.live.com/browse.aspx/yoursunny-study/IS206
downloads:
    - "00课程简介.ppt"
    - "01计算机系统发展回顾.ppt"
    - "02计算机系统概述.ppt"
    - "03进程管理.ppt"
    - "04进程通信.ppt"
    - "04进程通信b.ppt"
    - "05存储管理.ppt"
    - "06文件系统.ppt"
    - "07文件系统linux ext2.ppt"
    - "08 I0系统.ppt"
    - "Introduction to NachOS.ppt"
    - "课本01操作系统概论.pdf"
    - "课本02存储管理.pdf"
    - "课本03进程管理.pdf"
    - "课本04进程通信.pdf"
    - "课本05设备管理.pdf"
    - "课本06文件系统.pdf"
    - "Operating Systems, Internals and Design Principles.zip"
    - "nachos_cc.zip"
    - "nachos_java.zip"
---

* [NachOS开发日记](nachos-diary.pdf) 记录我在NachOS里度过的岁月
* [Implementing Priority Scheduler with Round-Robin on Nachos](nachos-scheduler.pdf)
* [Implementing Virtual Memory on Nachos](nachos-vm.pdf)
